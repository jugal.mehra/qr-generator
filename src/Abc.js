import React from 'react'
import VCard from 'vcard-creator'
function Abc() {
    // Define a new vCard
const myVCard = new VCard()

// Some variables
const lastname = 'Desloovere'
const firstname = 'Jeroen'
const additional = ''
const prefix = ''
const suffix = ''

myVCard
  // Add personal data
  .addName(lastname, firstname, additional, prefix, suffix)
  // Add work data
  .addCompany('Siesqo')
  .addJobtitle('Web Developer')
  .addRole('Data Protection Officer')
  .addEmail('info@jeroendesloovere.be')
  .addPhoneNumber(1234121212, 'PREF;WORK')
  .addPhoneNumber(123456789, 'WORK')
  .addAddress(null, null, 'street', 'worktown', null, 'workpostcode', 'Belgium')
  .addURL('http://www.jeroendesloovere.be')

console.log(myVCard.toString())


  return (
    <div>Abc</div>
  )
}

export default Abc