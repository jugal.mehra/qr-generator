import logo from "./logo.svg";
import "./App.css";
// import VCard from 'vcard-creator'
// import VCard from 'https://cdn.skypack.dev/vcard-creator'
import ReactDOM from "react-dom";
import { QRCodeSVG } from "qrcode.react";
import Abc from "./Abc";
import VCard from "vcard-creator";

function App() {
  const myVCard = new VCard();

  myVCard
    .addName("lastname", "firstname", "additional", "prefix", "suffix")
    .addCompany("Siesqo")
    .addJobtitle("Web Developer")
    .addRole("Data Protection Officer")
    .addEmail("info@jeroendesloovere.be")
    .addPhoneNumber(1234121212, "PREF;WORK")
    .addPhoneNumber(123456789, "WORK")
    .addAddress(
      null,
      null,
      "street",
      "worktown",
      null,
      "workpostcode",
      "Belgium"
    )
    .addURL("http://www.jeroendesloovere.be");

  return (
    <div>
      <center>
        {" "}
        <QRCodeSVG value={myVCard} />
      </center>
    </div>
  );
}

export default App;
